class Employee {
  constructor(name, surname, age, salary) {
      this.name = name;
      this.surname = surname;
      this.age = age;
      this.salary = salary;
  }
  get getFullName(){
    return `${this.name} + ${this.surname}`;
  }
  set getFullName(value) {
    [this.name, this.surname] = value.split(" ");
  }

}

class Programmer extends Employee {
  constructor(name, surname,  age, salary, lang = []) {
    super (name, surname, age, salary);
    this.lang = lang;
  }
  get () {
    return this.salary * 3;
  }
}

const programmer1 = new Programmer ('John', 'Smith', 23, 2000, ['eng', 'germ']);
console.log(programmer1);
console.log(programmer1.get());

const programmer2 = new Programmer ('Anna', 'German', 27, 1800, ['jan']);
console.log(programmer2);
console.log(programmer2.get());

const programmer3 = new Programmer ('Petter', 'Drawn', 18, 500, ['pl', 'fr', 'eng']);
console.log(programmer3);
console.log(programmer3.get()); 