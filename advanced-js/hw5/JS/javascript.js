const ipInfo = document.querySelector('.ip-info');
const ipBtn = document.querySelector('.btn');

async function getUserIpData() { 
  ipInfo.innerHTML = "";
  const getUserIP = await fetch(`http://api.ipify.org/?format=json`);
  const userIp = await getUserIP.json();
  const getUserAdres = await fetch(`http://ip-api.com/json/${userIp.ip}?fields=status,message,continent,country,region,city,district`);
  const userAdres = await getUserAdres.json();
  console.log(userAdres);
  ipInfo.insertAdjacentHTML('afterbegin', `
    <p><b>Континент: </b>${userAdres.continent}</p>
    <p><b>Cтрана: </b>${userAdres.country}</p>
    <p><b>Регион: </b>${userAdres.region}</p>
    <p><b>Город: </b>${userAdres.city}</p>
    <p><b>Район города: </b>${userAdres.district}</p>
  `);
}
ipBtn.addEventListener('click', getUserIpData)