const books = [
  { 
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70 
  }, 
  {
   author: "Скотт Бэккер",
   name: "Воин-пророк",
  }, 
  { 
    name: "Тысячекратная мысль",
    price: 70
  }, 
  { 
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70
  }, 
  {
   author: "Дарья Донцова",
   name: "Детектив на диете",
   price: 40
  },
  {
   author: "Дарья Донцова",
   name: "Дед Снегур и Морозочка",
  }
];

const newArrBooks = books.map(book => {
  const {author, name, price} = book;
  try {
    if (author && name && price) {
    return `<li><p>Author: ${author}</p><p>Book: ${name}</p><p>Price: ${price}$</p></li>`;
    } else if (!author) {
      throw new Error('Author is undefined')
    } else if (!name) {
      throw new Error('Name is undefined')
    } else if (!price) {
      throw new Error('Price is undefined')
    }
  } catch (e) {
      console.error(e);
    }
}); 
let newArrJoin = newArrBooks.join(" ");
const root = document.querySelector('#root')
let bookList = document.createElement('ul');
    root.prepend(bookList);
    bookList.insertAdjacentHTML('beforeend', newArrJoin);

