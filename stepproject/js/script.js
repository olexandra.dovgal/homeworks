const tabsTitle = document.querySelectorAll('.services-title');
const tabsContent = document.querySelectorAll('.services-par');
const tabs = document.querySelector('.services');
tabs.addEventListener('click', function (event) {
    let target = event.target;
        for (let i = 0; i < tabsTitle.length; i++) {
           if (target === tabsTitle[i]) {
               tabsTitle[i].classList.add('active');
               tabsContent[i].classList.add('show');
            } else {
                tabsTitle[i].classList.remove('active');
                tabsContent[i].classList.remove('show');
            }
}});



const btnWork = document.querySelector('.learn-more');
let counter = 12;
const workImg = document.querySelectorAll(".work-image img");
const workTitles = document.querySelectorAll(".work li");
let filter = "all";
document.querySelector(".work").addEventListener("click", (event) => {
    if (event.target.innerText.length < 30) {
        selectCategoryTab(event.target);
        filter = event.target.dataset.filter;
        showWorksByCategory(event.target.dataset.filter);
    }
});
function selectCategoryTab(target) {
    for (const title of workTitles) {
        if (title === target) {
            title.classList.add("selected");
        }
        else {
            title.classList.remove("selected");
        }
    }
}
btnWork.addEventListener("click", function(event) {
        counter += 12;
        if (counter === 24) {
            btnWork.classList.add("hide");
        }
        setTimeout(() => {
            showWorksByCategory(filter);
        }, 2000);
    }
);
function showWorksByCategory(filterName) {
    if (filterName === "all") {
        for (let i = 0; i < counter; i++) {
            workImg[i].classList.remove("hide");
        }
        return;
    }
    for (let i = 0; i < counter; i++) {
        if (workImg[i].dataset.filter === filterName) {
            workImg[i].classList.remove("hide");
        }
        else {
            workImg[i].classList.add("hide");
        }
    }
}
            
$(document).ready(function(){
    $('.slider').slick({
        arrows:true,
        dots:false,
        slidesToShow:(4),
        focusOnSelect: true,
        speed:3000,
        infinite: true,
        asNavFor:'.sliderbig',
        cssEasy: "linear",
    });
    $('.sliderbig').slick({
        arrows:false,
        fade:true,
        asNavFor:'.slider',
        cssEasy: "linear",
    })
});