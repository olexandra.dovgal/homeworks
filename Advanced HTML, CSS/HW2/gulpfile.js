const gulp = require('gulp');
const concat = require('gulp-concat');
const del = require('delete');
const minify = require('gulp-minifier');
const htmlmin = require('gulp-htmlmin');
const autoprefixer = require('gulp-autoprefixer');
const sass = require('gulp-sass');
const imagemin = require('gulp-imagemin');
const browserSync = require("browser-sync").create();

function buildCSS() {
    return gulp.src('src/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('style.css'))
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(gulp.dest('dist'))
        .pipe(browserSync.stream())
}

function buildHTML() {
    return gulp.src('src/*.html')
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest('dist'))
        .pipe(browserSync.stream());
}

function buildJS() {
    return gulp.src('src/*.js')
        .pipe (minify({
        minify: true,
        minifyJS: {
            sourceMap: true
        }}))
        .pipe(gulp.dest('dist'))
        .pipe(browserSync.stream());
}
function buildImages() {
    return gulp.src('src/img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/img'))
        .pipe(browserSync.stream());
}

function clean() {
    return del('dist/**', { force: true });
}
function browserSyncRefresh() {
    browserSync.init({
        server: { baseDir: "dist"},
        port: 3000,
        notify: false
    });
}

function build() {
    return gulp.series([
        clean,
        gulp.parallel([
            buildImages,
            buildCSS,
            buildHTML,
            buildJS
        ])
    ])
}

function start() {
    gulp.watch('src/**/*', build())
}

function watchFiles() {
            gulp.watch('src/*.html', buildHTML);
            gulp.watch('src/scss/*.scss', buildCSS);
            gulp.watch('src/*.js', buildJS);
            gulp.watch('src/img/*', buildImages);
        }       
let dev = gulp.parallel(build, watchFiles, browserSyncRefresh);

exports.buildHTML = buildHTML;
exports.buildCSS = buildCSS;
exports.buildJS = buildJS;
exports.clean = clean;
exports.start = start;
exports.build = build();
exports.default = dev;