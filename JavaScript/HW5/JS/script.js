function createNewUser() {
let firstName = prompt("Введіть ім'я");
let lastName = prompt("Введіть прізвище");
let birthDate = prompt("Введіть дату народження у форматі dd.mm.yyyy");
const nowDate = new Date();
let arr = birthDate.split(".");

const newUser = {
  name: firstName,
  surname: lastName,
  getLogin: function () {
      let login = (this.name[0] + this.surname).toLowerCase();
    return login;
  },
  birthday: new Date(arr[2], arr[1], arr[0]),
  getAge: function() {
    let age = ((nowDate - this.birthday)/(24 * 3600 * 365.25 * 1000));
    return Math.floor(age);
  },
  getPassword: function() {
    let paswoword = this.name[0].toUpperCase() + this.surname.toLowerCase() + arr[2];
    return paswoword;
  },
};
return newUser;
};

// newUser, newUser.getAge(), newUser.getPassword()
let user = createNewUser();
console.log(user);
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());



