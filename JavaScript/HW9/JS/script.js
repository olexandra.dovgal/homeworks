let tabsTitle = document.querySelectorAll('.tabs-title');
let tabsContent = document.querySelectorAll('.tabs-par');
let tabs = document.querySelector('.tabs');
tabs.addEventListener('click', function (event) {
    let target = event.target;
        for (let i = 0; i < tabsTitle.length; i++) {
           if (target === tabsTitle[i]) {
               tabsTitle[i].classList.add('active');
               tabsContent[i].classList.add('tabs-par-show');
            } else {
                tabsTitle[i].classList.remove('active');
                tabsContent[i].classList.remove('tabs-par-show');
            }
}});