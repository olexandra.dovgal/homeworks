const btn = document.querySelector('.theme-btn');
const body = document.getElementsByTagName('body');
let theme = localStorage.getItem('theme'); 
window.onload = function () {
    if (localStorage.getItem('theme') !== null) {
        body[0].classList = theme;
}}
btn.addEventListener('click', function(event) {
    if (localStorage.getItem('theme')){
        body[0].classList.remove('theme');
        localStorage.removeItem('theme');
    } else {
        body[0].classList.add('theme');
        localStorage.setItem('theme', 'Coral')
    };
})