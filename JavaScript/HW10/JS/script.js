let btn = document.querySelector('.btn');
let inputPassword = document.querySelector('.password');
let inputConfirmPassword  = document.querySelector('.confirm-password');
let iconPassword = document.querySelector('.fa-eye-slash');
let iconPasswordConfirm = document.querySelector('.icon-password-confirm');
let par = document.createElement('p');
function showPassword(input, icon) {
    par.innerHTML = "";
    if (input.getAttribute('type') === 'password') {
        input.setAttribute('type', 'text');
        icon.classList.remove('hide');
    } else {
        icon.classList.add('hide');
        input.setAttribute('type', 'password')
    }
};
inputPassword.addEventListener('click', function(event) {
    showPassword(inputPassword, iconPassword)});
inputConfirmPassword.addEventListener('click', function(event) {
    showPassword(inputConfirmPassword, iconPasswordConfirm)});
btn.addEventListener('click', function (event) {
    if (inputPassword.value === inputConfirmPassword.value && inputConfirmPassword.value !== "") {
        alert('You are welcome');
    } else {
        par.textContent = ('Нужно ввести одинаковые значения');
        par.style.color = 'red';
        inputConfirmPassword.after(par);
    }
})