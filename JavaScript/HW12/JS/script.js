const img = document.querySelectorAll('.image-to-show');
let current = 0;
let imgInterval;

function start () {
    imgInterval = setInterval( function () {
    img[current].classList = 'hide'
    current = (current + 1) % img.length;
    img[current].className = 'image-to-show';
}, 3000)}
start();

const btnStop = document.querySelector('.stop');
const btnStart = document.querySelector('.start');
btnStop.addEventListener('click', function () {
    clearInterval(imgInterval);
})
btnStart.addEventListener('click', function () {
    start();
})

