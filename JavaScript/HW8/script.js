// При загрузке страницы показать пользователю поле ввода (input) с надписью Price. Это поле будет служить для ввода числовых значений
// Поведение поля должно быть следующим:

// При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.
// Когда убран фокус с поля - его значение считывается, над полем создается span, в котором должен быть выведен текст: Текущая цена: ${значение из поля ввода}. Рядом с ним должна быть кнопка с крестиком (X). Значение внутри поля ввода окрашивается в зеленый цвет.
// При нажатии на Х - span с текстом и кнопка X должны быть удалены. Значение, введенное в поле ввода, обнуляется.
// Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой, под полем выводить фразу - Please enter correct price. span со значением при этом не создается.


// В папке img лежат примеры реализации поля ввода и создающегося span.
const div = document.querySelector('div');
div.style.display = ('flex');
const inputEl = document.querySelector("#price");
const span = document.createElement("span");
const xBtn = document.createElement("button");
const spanUncorrect = document.createElement("span");
inputEl.addEventListener('focus', function() {
    this.style.outline = ('none');
    this.style.border = ('3px solid green');
});
inputEl.addEventListener('blur', function() {
    span.innerText = "";
    xBtn.display = ('none');
    spanUncorrect.inputText = "";
    this.style.border = '1px solid black';
});
function addItem() {
    const inputText = inputEl.value;
    if (inputText > 1 && typeof number) {
    span.innerText = `Текущая цена: ${inputText}`;
    xBtn.innerText = "X";
    inputEl.style.backgroundColor = ('green');
    document.body.prepend(span);
    span.append(xBtn);
xBtn.addEventListener('click', function (event) {
        span.style.display = 'none';
        inputEl.value = "";
        inputEl.style.backgroundColor = ('white');
    })} else {
        spanUncorrect.innerHTML = "Please enter correct price";
        inputEl.style.border = ('3px solid red');
        inputEl.style.backgroundColor = ('white');
        document.body.append(spanUncorrect);
    };
}
inputEl.addEventListener("blur", addItem);


