import React from 'react'
import {useDispatch} from 'react-redux'
import {setForm, clearStorage} from '../../store/form/actions'
import { Formik, Form, Field, ErrorMessage } from 'formik';
import Validate from './Validation'

const CartForm = () => {
    const dispatch = useDispatch()
    // const handleSubmitTo = (values) => {
    //     dispatch(setForm());
    //     dispatch(clearStorage());
    // }
    return(
    <div>
        <h1>Form</h1>
        <Formik
        initialValues={{ 
            firstName: '',
            lastName: '',
            age: '',
            address: '',
            phoneNumber: '', 
        }}
        validationSchema={Validate}
        onSubmit={(values) => {
            dispatch(setForm(values));
            dispatch(clearStorage());
            console.log(values)
        }}
        >
        {({
            handleSubmit
        }) =>  (
                <Form  onSubmit={handleSubmit}>
                <div className="form-group align-items-center">
                    <Field type="text" name="firstName" placeholder="FirstName" />
                    <ErrorMessage name="firstName" />
                    <Field type="text" name="lastName" placeholder="LastName" />
                    <ErrorMessage name="lastName" />
                    <Field type="number" name="age" placeholder="Age" />
                    <ErrorMessage name="age" />
                    <Field type="text" name="address" placeholder="Address" />
                    <ErrorMessage name="address" />
                    <Field type="text" name="phoneNumber" placeholder="PhoneNumber" />
                    <ErrorMessage name="phoneNumber"/>
                </div>
                  <button className="btn-light checkout" type="submit">Checkout</button>
                </Form>
            ) 
        }
        </Formik>
    </div>
)};
export default CartForm