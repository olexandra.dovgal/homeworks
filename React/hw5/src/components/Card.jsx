import React, { useEffect, useState } from 'react';
import Button from './Button';
import PropTypes from 'prop-types'


 const Card = (props) => {

    // state = {
    //     starColor: 'black',
    // }
    const [starColor, setStarColor] = useState('black');
    useEffect(() => {
        const articleArr = localStorage.getItem('favs');
        const parsedArr = JSON.parse(articleArr)
        if (parsedArr) {
            parsedArr.forEach(element => {
                if (element === props.article) {
                    setStarColor('violet')
                }
            })
        } 
    }, [])

    const drawColor = () => {

        if(starColor === 'black') {
            setStarColor('violet')
        } else if(starColor === 'violet') {
            setStarColor('black')
        }

        props.onClickStar()
    }

        const {article, name, image, price, productColor, onClickAddToCart} = props
        return (
            <>
            <li key={article} className="card" style={{width: "18rem"}}>
                <img src={image} alt='clothes' className="card-img-top" style={{width:'100%', height:'382px'}}/>
                <div className="card-body">
                    <div>
                        <p className="text-secondary"> Name: {name} </p>
                        <p className="text-secondary"> Price: {price} uan </p>
                        <p className="text-secondary"> Color: {productColor} </p>
                    </div>
                    <div>
                        {
                            !props.isCart && 
                            <div onClick={drawColor} className="svg-star"> 
                                <svg  xmlns="http://www.w3.org/2000/svg" style={{width:'20px', height:'20px'}} className="bi bi-star" viewBox="0 0 16 16">
                                    <path style={{fill: starColor}} d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.522-3.356c.33-.314.16-.888-.282-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288L8 2.223l1.847 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.565.565 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z"/>
                                </svg>    
                            </div>
                        }
                        <div>
                            <Button className='btn btn-light' text={!props.isCart ? "Add to cart" : "Remove from the cart"} onClick={onClickAddToCart}/>
                        </div>
                    </div>
                </div>
            </li>
            </>
            )
}

Card.propTypes = {
    onClick: PropTypes.func,
    name: PropTypes.string,
    image: PropTypes.string,
    article: PropTypes.number,
    productColor: PropTypes.string,
    onClickStar: PropTypes.func,
    onClickAddToCart: PropTypes.func,
    onClickSaveToCart: PropTypes.func,
  };

  export default Card