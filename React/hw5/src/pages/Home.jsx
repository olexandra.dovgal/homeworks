import React, {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux'
import ProductCard from '../containers/ProductCard';
import fetchCards from '../store/card/actions'

const Home = () => {
  const dispatch = useDispatch()
  const clothes = useSelector(store => store.cardReducer.clothes)
  

  useEffect(() => {
    dispatch(fetchCards())
  },[dispatch])


        return (
                <ProductCard clothesList={clothes}/>
        )
    }


export default Home

