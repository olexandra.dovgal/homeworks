import React, {useState, useEffect} from 'react'
import ProductCard from '../containers/ProductCard'
import fetchCards from '../store/card/actions'
import {useSelector, useDispatch} from 'react-redux'
import CartForm from '../components/Form/CartForm'

const Cart = (props) => {
    const dispatch = useDispatch()
    const clothes = useSelector(store => store.cardReducer.clothes)
    
  
    useEffect(() => {
      dispatch(fetchCards())
    },[dispatch])

    const [cartClothes, setCartClothes] = useState([])
    const articleArr = localStorage.getItem('cart');
    const parsedArr = JSON.parse(articleArr)

    useEffect(() => {
        
        if (parsedArr) {
            setCartClothes(
                clothes.filter(obj => parsedArr.find(id => id === obj.article))
            )
        }
            }, [clothes])
         
    
    return (
        <div>
          <h1>Cart :</h1>
          {(cartClothes.length > 0) ? 
          <>
          <ProductCard isCart clothesList={cartClothes}/>
          <CartForm />
          </> : <h2 className="no-item">No item in cart</h2>}
        </div>
    )
}

export default Cart