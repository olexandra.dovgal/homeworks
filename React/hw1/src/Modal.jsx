import React, { Component } from 'react'
import Button from './Button'

export class Modal extends Component {

    render() {
        const {classNameHeader, className, header, closeButton, text, onClick, stopPropagation, action} = this.props
        return (
        <div className="modal-container"  onClick={onClick}>
            <div className="modal-dialog modal-dialog-centered">
              <div className={className} onClick={stopPropagation}>
                <div className={classNameHeader}>
                  <h5 className="modal-title" >{header}</h5>
                  {closeButton && <Button className="close" text='X' onClick={onClick} />}
                </div>
                <div className="modal-body">
                  <p>{text}</p>
                </div>
                <div className="modal-footer">
                  {action}
                </div>
              </div>
            </div>
        </div> 
        )
    }
}

export default Modal

{/* <div className="modal-dialog">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">{header}</h5>
                        <Button className="close" text='X'></Button>
                    </div>
                    <div className="modal-body">
                        <p>{text}</p>
                    </div>
                    <div className="modal-footer">
                        <Button className="btn btn-secondary" text='Ok'></Button>
                        <Button className="btn btn-primary" text='Cancel'></Button>
                    </div>
                    </div>
                </div> */}