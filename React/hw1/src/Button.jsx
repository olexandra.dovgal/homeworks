import React from 'react'


class Button extends React.PureComponent {
  constructor() {
    super();
    this.state = {
      title: 'Hello world from constr',
    }
  }
 
  
 
  render () { 
    const { className, backgroundColor, text, onClick} = this.props
    return (
      <div>
        <button className={className} style= {{backgroundColor: backgroundColor}} onClick={onClick}>{text}</button>
      </div>
    )
  }
}

export default Button;
