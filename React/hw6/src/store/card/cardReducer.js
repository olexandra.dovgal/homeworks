import * as types from '../types'

const initalState = {
    clothes: []
};

const cardReducer = (state = initalState, action) => {
    switch (action.type) {
        case types.SET_CARDS:
            return {
                ...state,
                clothes: action.payload
            }
        default: 
        return state
    }
}


export default cardReducer