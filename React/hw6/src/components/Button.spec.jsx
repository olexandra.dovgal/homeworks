import Button from './Button';
import { render, fireEvent, screen } from '@testing-library/react'

describe('Button component', () => {
    it('should render correctly', () => {
        render(<Button/>)
    })

    it('should render correctly by name', () => {
        const testTitle = 'Add to cart'
        const { getByText } = render(<Button text={testTitle} />)
        getByText(testTitle)
    
    })

    it('should add class', () => {
        const className = 'light';
        const { queryByRole } = render(<Button className={className}>Add to cart</Button>)
        const button = queryByRole('button')
        expect(button.className).toEqual(className)
    })

    it('should pass click', () => {
        const onClick = jest.fn()

        const {queryByRole} = render(<Button onClick={onClick}>Add to cart</Button>)
        fireEvent(
            queryByRole('button'),
            new MouseEvent('click', { bubbles: true})
        )
        
        expect(onClick).toHaveBeenCalled()
    })
})