import React from 'react';
import PropTypes from 'prop-types'


const Button = (props) => {
 
  
    const { className, backgroundColor, text, onClick} = props
    return (
      <div>
        <button className={className} style= {{backgroundColor: backgroundColor}} onClick={onClick}>{text}</button>
      </div>
    )
  }

Button.propTypes = {
  onClick: PropTypes.func,
  text: PropTypes.string
}


export default Button;
