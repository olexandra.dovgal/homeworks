import Modal from './Modal';
import { render, fireEvent, screen } from '@testing-library/react'
import userEvent from "@testing-library/user-event"

describe('Modal component', () => {
    it('should render correctly', () => {
        render(<Modal/>)
    })

    
    it('should add text', () => {
        const text = 'test';
        const {getByText} = render(<Modal text={text} />)
        getByText(text)

    })

})