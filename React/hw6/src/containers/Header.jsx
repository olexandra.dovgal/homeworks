import React from 'react'
import { Router, Link } from 'react-router-dom'

export default function Header() {
    return (
        <div className="menu">
            <Link to="/" className="menu-item">Home</Link> 
            <Link to="/cart" className="menu-item">Cart</Link> 
            <Link to="/favorites" className="menu-item">Favorites</Link> 
        </div>
    )
}