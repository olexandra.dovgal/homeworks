import * as types from '../types'

const initalState = {
    modalIsOpen: false
};

const modalReducer = (state = initalState, action) => {
    switch (action.type) {
        case types.SHOW_MODAL:
            return {
                modalIsOpen: action.payload
            }
        case types.CLOSE_MODAL:
            return {
                modalIsOpen: action.payload
            }
        default: 
        return state
    }
}


export default modalReducer