import React, {useState, useEffect} from 'react'
import ProductCard from '../containers/ProductCard'
import fetchCards from '../store/card/actions'
import {useSelector, useDispatch} from 'react-redux'

const Favorites = () => {
    const dispatch = useDispatch()
    const clothes = useSelector(store => store.cardReducer.clothes)
    
    useEffect(() => {
      dispatch(fetchCards())
    },[dispatch])
    
    const [favsClothes, setFavsClothes] = useState([])
    const articleArr = localStorage.getItem('favs');
    const parsedArr = JSON.parse(articleArr)
   
    
        useEffect(() => {
            if (parsedArr) {
                setFavsClothes(
                    clothes.filter(obj => parsedArr.find(id => id === obj.article))
                )
            } else {
                setFavsClothes(false)
            }
        },[clothes, parsedArr])

    return (
        <div>
          <h1>Favorites :</h1>
          {favsClothes && <ProductCard isFav clothesList={favsClothes}/>}
        </div>
    )
}

export default Favorites