import React, {useState, useEffect} from 'react'
import server from '../api/serves'
import ProductCard from '../containers/ProductCard'
const Favorites = (props) => {
    const clothes = server ()
    const [cartClothes, setCartClothes] = useState([])
    const articleArr = localStorage.getItem('cart');
    const parsedArr = JSON.parse(articleArr)

    useEffect(() => {
        
        if (parsedArr) {
            setCartClothes(
                clothes.filter(obj => parsedArr.find(id => id === obj.article))
            )
        }
            }, [parsedArr])
         
    
    return (
        <div>
          <h1>Cart :</h1>
          {cartClothes.length > 0 && <ProductCard isCart clothesList={cartClothes}/>}
        </div>
    )
}

export default Favorites