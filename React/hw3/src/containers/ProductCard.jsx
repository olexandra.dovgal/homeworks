import React, { useState } from 'react'
import Card from '../components/Card'
import Button from '../components/Button'
import Modal from '../components/Modal'
import PropTypes from 'prop-types'

const ProductCard = (props) => {

  console.log(props.clothesList);
   
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [productArticle, setProductArticle] = useState(null);
    const [isFavClothes, setIsFavClothes] = useState(false)
    const addToFav = (id) => {
        const favArray = localStorage.getItem('favs')

        if(favArray) {
            const parsedArr = JSON.parse(favArray)

            const indexFav = parsedArr.indexOf(id)

            if (indexFav === -1) {
                parsedArr.push(id)
                localStorage.setItem('favs', JSON.stringify(parsedArr))
                setIsFavClothes(true)
            } else {
                const spliced = parsedArr.splice(indexFav, 1)
                localStorage.setItem('favs',JSON.stringify(parsedArr))
                setIsFavClothes(false)
            }

        } else {
            localStorage.setItem('favs',JSON.stringify([id]))
        }
 
    }

    const getId = id => {
      setProductArticle(id)
      showModal()
    }

    const showModal = () => {
      setIsModalOpen(true)
      }
    const closeModal = () => {
        if (isModalOpen) {
          setIsModalOpen(false)
        } 
      }
    const saveToCart = (id) => {
        const cartArray = localStorage.getItem('cart')
        if(cartArray) {
            const parsedArr = JSON.parse(cartArray)
            const indexCart = parsedArr.indexOf(id)
            if (indexCart === -1) {
                parsedArr.push(id)
                localStorage.setItem('cart', JSON.stringify(parsedArr))
            } 

        } else {
            localStorage.setItem('cart',JSON.stringify([id]))
        }
      }
      const removeFromCart = (id) => {
        const cartArray = localStorage.getItem('cart')
        if(cartArray) {
            const parsedArr = JSON.parse(cartArray)
            const indexCart = parsedArr.indexOf(id)
            parsedArr.splice(indexCart, 1)
            localStorage.setItem('cart', JSON.stringify(parsedArr))
        } 
      }

        const arrClothes = props.clothesList.map((item) => {
            return (
                <Card 
                key={item.article} 
                name={item.name} 
                article={item.article} 
                image={item.image}
                price={item.price}
                productColor={item.color}
                onClickStar={() => addToFav(item.article)} 
                onClickAddToCart={() => getId(item.article)} 
                favClothes={isFavClothes}
                isCart={props.isCart}/>
            )
        })

        return (
            <>
                <ul>
                     {arrClothes}
                </ul>
            
            {isModalOpen &&
                <Modal 
                classNameHeader='modal-header'
                className='modal-content'
                  header='Do you want to add to cart?' 
                  closeButton={true} 
                  text="Are you sure you want to add to cart?" 
                  onClick={closeModal}
                  stopPropagation={props.stopPropagation}
                  action={
                    <>
                      <div className='button-container'>
                        <Button className="btn btn-secondary" text='Ok' onClick={() => {
                          props.isCart ? removeFromCart(productArticle) : saveToCart(productArticle)
                        }}></Button>
                        <Button className="btn btn-primary" text='Cancel' onClick={closeModal}></Button>
                      </div>
                    </>}
                  />
              } 
              {(isModalOpen && props.isCart) &&
              <Modal 
                classNameHeader='modal-header header-red'
                className='modal-content red '
                header='Do you want to delete this file?' 
                closeButton={true}  
                text="Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?" 
                onClick={closeModal} 
                stopPropagation={props.stopPropagation}
                action={
                  <>
                    <div className='button-container'>
                      <Button className="btn btn-danger" text='Ok' onClick={() => {
                          props.isCart ? removeFromCart(productArticle) : saveToCart(productArticle)
                        }}></Button>
                      <Button className="btn btn-danger" text='Cancel' onClick={closeModal}></Button>
                    </div>
                  </>}
              />}
            </>
        )
    }

ProductCard.propTypes = {
  clothes: PropTypes.array
};

export default ProductCard
