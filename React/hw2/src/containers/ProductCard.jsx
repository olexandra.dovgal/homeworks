import React, { Component } from 'react'
import Card from '../components/Card'
import Button from '../components/Button'
import Modal from '../components/Modal'
import PropTypes from 'prop-types'

export class ProductCard extends Component {
    
    state = {
        isModalOpen: false,
        productArticle: null
    }
    addToFav = (id) => {
        const favArray = localStorage.getItem('favs')

        if(favArray) {
            const parsedArr = JSON.parse(favArray)

            const indexFav = parsedArr.indexOf(id)

            if (indexFav === -1) {
                parsedArr.push(id)
                localStorage.setItem('favs', JSON.stringify(parsedArr))
            } else {
                const spliced = parsedArr.splice(indexFav, 1)
                console.log(spliced);
                localStorage.setItem('favs',JSON.stringify(parsedArr))
            }

        } else {
            localStorage.setItem('favs',JSON.stringify([id]))
        }
 
    }

    getId = id => {
        this.setState({productArticle: id})
        this.showModal()
    }

    showModal = () => {
        this.setState({
          isModalOpen: true,
        //   productArticle: this.props.article
        })
      }
    closeModal = () => {
        if (this.state.isModalOpen) {
          this.setState({
            isModalOpen: false
          })
        } 
      }
    saveToCart = (id) => {
        const cartArray = localStorage.getItem('cart')
        if(cartArray) {
            const parsedArr = JSON.parse(cartArray)

            const indexCart = parsedArr.indexOf(id)

            if (indexCart === -1) {
                parsedArr.push(id)
                localStorage.setItem('cart', JSON.stringify(parsedArr))
            } 

        } else {
            localStorage.setItem('cart',JSON.stringify([id]))
        }
      }


    render() {

        const arrClothes = this.props.clothesList.map((item, index) => {
            return (
                <Card 
                key={item.article} 
                name={item.name} 
                article={item.article} 
                image={item.image}
                price={item.price}
                productColor={item.color}
                onClickStar={() => this.addToFav(item.article)} 
                onClickAddToCart={() => this.getId(item.article)} 
                onClickSaveToCart={() => this.saveToCart(item.article)}/>
                
            )
        })

        return (
            <>
                <ul>
                     {arrClothes}
                </ul>
            
            {this.state.isModalOpen &&
                <Modal 
                classNameHeader='modal-header'
                className='modal-content'
                  header='Do you want to save this file?' 
                  closeButton={true} 
                  text="Are you sure you want to save this file?" 
                  onClick={this.closeModal}
                  stopPropagation={this.stopPropagation}
                  action={
                    <>
                      <div className='button-container'>
                        <Button className="btn btn-secondary" text='Ok' onClick={() => this.saveToCart(this.state.productArticle)}></Button>
                        <Button className="btn btn-primary" text='Cancel' onClick={this.closeModal}></Button>
                      </div>
                    </>}
                  />
              } 
            </>
        )
    }
}

ProductCard.propTypes = {
  clothes: PropTypes.array
};

export default ProductCard
