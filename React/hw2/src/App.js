import React from 'react';
import Home from './pages/Home'


class App extends React.Component {
    constructor(props) {
      super(props)
      this.state = {
        isFirstModalOpen: false,
        isSecondModalOpen: false,
        clothes: [],
      }
    }
    
    stopPropagation = (e) => {
      e.stopPropagation()
    }
    showFirstModal = () => {
      this.setState({
        isFirstModalOpen: true
      })
    }
    
    closeFirstModal = () => {
      if(this.state.isFirstModalOpen) {
        this.setState({
          isFirstModalOpen: false
        })
      } 
    }
    
    render() {
      return (
          <>
          <Home />
          {/* <div className="button-container">
              <Button  className='open-button' text='Open first modal' backgroundColor='blue' onClick={this.showFirstModal}/>
              <Button  className='open-button' text='Open second modal' backgroundColor='yellow' onClick={this.showSecondModal}/>
          </div> */}
            {/* {this.state.isFirstModalOpen && 
              <Modal 
                classNameHeader='modal-header header-red'
                className='modal-content red '
                header='Do you want to delete this file?' 
                closeButton={true}  
                text="Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?" 
                onClick={this.closeFirstModal} 
                stopPropagation={this.stopPropagation}
                action={
                  <>
                    <div className='button-container'>
                      <Button className="btn btn-danger" text='Ok'></Button>
                      <Button className="btn btn-danger" text='Cancel' onClick={this.closeFirstModal}></Button>
                    </div>
                  </>}
              />} */}
            
          
        </>
        
      )
    }
  }
  
  export default App;
  