import React, { Component } from 'react'
import ProductCard from '../containers/ProductCard'

export class Home extends Component {
    constructor(props) {
        super(props)
        this.state = {
          clothes: [],
        }
      }
    componentDidMount(){
        fetch('database.json')
        .then (response => response.json())
        .then (data => this.setState({clothes: data}))
        ;
      }
    render() {

        return (
            <>
                <ProductCard clothesList={this.state.clothes}/>
            </>
        )
    }
}

export default Home
